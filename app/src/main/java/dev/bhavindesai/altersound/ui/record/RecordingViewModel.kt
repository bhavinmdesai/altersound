package dev.bhavindesai.altersound.ui.record

import android.os.CountDownTimer
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hadilq.liveevent.LiveEvent
import java.util.*

class RecordingViewModel : ViewModel() {

    private val _checkPermission = LiveEvent<Boolean>()
    val checkPermission:LiveData<Boolean> = _checkPermission

    private val _launchSettings = MutableLiveData<Boolean>()
    val launchSettings:LiveData<Boolean> = _launchSettings

    val timerText = MutableLiveData<String>()

    val isRecording = MutableLiveData<Boolean>()
    val visibilityTimer = MutableLiveData<Boolean>()
    val visibilitySoundWaves = MutableLiveData<Boolean>()
    val visibilityRecordButton = MutableLiveData<Boolean>()
    val visibilityOpenSettings = MutableLiveData<Boolean>()
    val visibilityPermissionExplanation = MutableLiveData<Boolean>()

    init {
        visibilityTimer.value = false
        visibilitySoundWaves.value = false
        visibilityRecordButton.value = false
        visibilityOpenSettings.value = false
        visibilityPermissionExplanation.value = false
    }

    fun openSettings() {
        _launchSettings.value = true
    }

    fun askPermission() {
        _checkPermission.value = true
    }

    fun setPermissionsGranted() {
        _checkPermission.value = false
        visibilityRecordButton.value = true
        visibilityPermissionExplanation.value = false
    }

    fun setPermissionsDenied() {
        _checkPermission.value = false
        visibilityOpenSettings.value = true
    }

    fun showPermissionExplanation(){
        _checkPermission.value = false
        visibilityPermissionExplanation.value = true
    }

    fun startRecording() {
        isRecording.value = true

        visibilityRecordButton.value = false
        visibilityTimer.value = true

        object : CountDownTimer(10_000, 100) {
            override fun onTick(millisUntilFinished: Long) {
                timerText.value = "%.1f\nseconds".format(Locale.US, millisUntilFinished / 1000F)
            }

            override fun onFinish() {
                isRecording.value = false

                visibilityTimer.value = false
                visibilitySoundWaves.value = true
            }
        }.start()
    }

    fun onAudioPlaybackComplete() {
        visibilityRecordButton.value = true
        visibilitySoundWaves.value = false
    }
}