package dev.bhavindesai.altersound.ui.record

import android.Manifest
import android.content.Intent
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.text.Spannable
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.github.squti.androidwaverecorder.WaveRecorder
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import dev.bhavindesai.altersound.R
import dev.bhavindesai.altersound.databinding.FragmentRecordingBinding
import net.surina.soundtouch.SoundTouch
import java.io.File


class RecordingFragment : Fragment() {

    private lateinit var binding:FragmentRecordingBinding

    private val inputFile by lazy {
        File(requireContext().filesDir, "input.wav")
    }

    private val outputFile by lazy {
        File(requireContext().filesDir, "output.wav")
    }

    private lateinit var waveRecorder: WaveRecorder

    private var permissionToken: PermissionToken? = null

    private val viewModel by viewModels<RecordingViewModel> ()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_recording,
            container,
            false
        )
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        setObservers()

        checkPermission()

        return binding.root
    }

    private fun setObservers() {
        viewModel.checkPermission.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            it?.let {
                if (it) {
                    if (permissionToken != null)
                        permissionToken?.continuePermissionRequest()
                    else
                        checkPermission()
                }
            }
        })

        viewModel.timerText.observe(viewLifecycleOwner, Observer {
            it?.let {
                val spannableString = SpannableString(it)

                val indexOfNewLine = it.indexOf("\n")

                spannableString.setSpan(
                    RelativeSizeSpan(2f),
                    0, indexOfNewLine,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )

                binding.txtTimer.text = spannableString
            }
        })

        viewModel.launchSettings.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                Intent().apply {
                    action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    data = Uri.fromParts(
                        "package",
                        requireActivity().packageName,
                        null
                    )

                    requireContext().startActivity(this)
                }
            }
        )

        viewModel.isRecording.observe(viewLifecycleOwner, Observer {
            it?.let { isRecording ->
                if (isRecording) {
                    waveRecorder = WaveRecorder(inputFile.absolutePath)
                    waveRecorder.startRecording()
                } else {
                    waveRecorder.stopRecording()

                    processAudio()

                    playAudio()
                }
            }
        })
    }

    private fun checkPermission() {
        Dexter.withActivity(requireActivity())
            .withPermission(Manifest.permission.RECORD_AUDIO)
            .withListener(object : PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    response?.let {
                        if (it.permissionName != null)
                            viewModel.setPermissionsGranted()
                    }
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                    response?.let {
                        if (it.isPermanentlyDenied) {
                            viewModel.setPermissionsDenied()
                        } else {
                            permissionToken?.cancelPermissionRequest()
                            permissionToken = null
                            viewModel.showPermissionExplanation()
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                )  {
                    permissionToken = token
                    permissionToken?.continuePermissionRequest()
                    viewModel.showPermissionExplanation()
                }

            })
            .check()
    }

    private fun processAudio() {
        SoundTouch().apply {
            setPitchSemiTones(6F)
            processFile(inputFile.absolutePath, outputFile.absolutePath)
            close()
        }
    }

    private fun playAudio() {

        MediaPlayer().apply {
            setAudioAttributes(
                AudioAttributes
                    .Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()
            )

            setDataSource(outputFile.absolutePath)

            setOnCompletionListener {
                viewModel.onAudioPlaybackComplete()

                requireActivity()
                    .window
                    .clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

                it.release()
            }

            prepare()
            start()
        }
    }
}