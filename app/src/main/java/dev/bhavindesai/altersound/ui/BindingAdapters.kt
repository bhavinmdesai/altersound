package dev.bhavindesai.altersound.ui

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("app:isVisible")
fun setVisibility(view: View?, visibility: Boolean?) {

    view?.let { v ->
        visibility?.let { isVisible ->
            v.visibility = if (isVisible) View.VISIBLE else View.GONE
        }
    }
}